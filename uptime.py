#!/usr/sbin/python3 

import uptime 

import os 

uptime_info = os.times()

days = int(uptime_info[0] / 86400)
hours = int((uptime_info[0] % 86400) / 3600)
minutes = int((uptime_info[0] % 3600) / 60)
seconds = int(uptime_info[0] % 60)

print(f"Uptime: {days} days, {hours} hours, {minutes} minutes, {seconds} seconds")

