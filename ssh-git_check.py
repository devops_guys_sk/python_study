#!/usr/bin/python3

import paramiko

def check_ssh_keys(servers, username, key_file):



    try:
       client = paramiko.SSHClient()
       client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
       client.connect(server, username=username, key_filename=key_file)
       client.close()
       return "ok"
    except paramiko.SSHException:
       return "no ok"


if __name__ == "__main__":
     # info about server
     server = "gitlab.com"
     username = "git"
     key_file = "/root/.ssh/id_rsa"

     # check status
     status = check_ssh_keys(server, username, key_file)
     print(status)

